# FileCompressor

Repo for file compressor
compressing file into ZIP

```
Input : folder path
Output : _dirname + '/output/' + Date.now() + 'output.zip'
```

msg body
```
{
    "correlationID" : correlationID,
    "path" : Output,
    "query_type" : "compress"
}
```

## How to Install

### Prerequisites
- Node.Js
- Node Package Manager (npm)
[Install both here](https://nodejs.org/en/download/)

### Starting the server
1. If it's the first time running the server please install all the packages needed by doing
```
npm install
```
2. Run the server by
```
npm start
```
