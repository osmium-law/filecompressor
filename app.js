// require modules
const amqp = require('amqplib');
const fs = require('fs');
const path = require('path');
const archiver = require('archiver');
const Winston = require('winston');
const WinstonLogStash = require('winston3-logstash-transport');

const logger = Winston.createLogger();

logger.add(new WinstonLogStash({
    mode: 'tcp',
    host: 'elk.faishol.net',
    port: 5000
}));

const RABBITMQ = 'amqp://osmium:osmium12345678@rabbitmq.faishol.net:5672';

const open = require('amqplib').connect(RABBITMQ);
const q = 'compressor_queue';

//Define Function to compress folder files
async function compressFolderFiles(input) {
    const promise = new Promise(resolve => {
        // Set input and output file path
        const outputpath = path.resolve(process.env.OUTPUT_FOLDER, Date.now() + 'output.zip');

        // Create a file to stream archive data to.
        const output = fs.createWriteStream(outputpath);
        const archive = archiver('zip', {
            zlib: {
                level: 9 // Sets the compression level.
            }
        });

        output.on('close', function () {
            logger.log({
                level: "info",
                message: archive.pointer() + ' total bytes'
            });
            logger.log({
                level: "info",
                message: 'archiver has been finalized and the output file descriptor has closed.'
            });
            resolve(outputpath);
        });
        // This event is fired when the data source is drained no matter what was the data source.
        // It is not part of this library but rather from the NodeJS Stream API.
        // @see: https://nodejs.org/api/stream.html#stream_event_end
        output.on('end', function () {
            console.log('Data has been drained');
        });

        // good practice to catch warnings (ie stat failures and other non-blocking errors)
        archive.on('warning', function (err) {
            if (err.code === 'ENOENT') {
                // log warning
            } else {
                // throw error
                throw err;
            }
        });

        // good practice to catch this error explicitly
        archive.on('error', function (err) {
            throw err;
        });

        // Pipe archive into output and insert files from input
        archive.pipe(output);
        archive.glob('*', {
            cwd: input
        });

        // Output the file
        archive.finalize();
    });
    const result = await promise;
    return result;
}

// Consumer
open
    .then(function (conn) {
        console.log(`[ ${new Date()} ] Server started`);
        return conn.createChannel();
    })
    .then(function (ch) {
        return ch.assertQueue(q).then(function (ok) {
            return ch.consume(q, function (msg) {
                console.log(
                    `[ ${new Date()} ] Message received: ${JSON.stringify(
            JSON.parse(msg.content.toString('utf8')),
          )}`,
                );
                if (msg !== null) {
                    const request = JSON.parse(msg.content.toString('utf8'))
                    compressFolderFiles(request.folder).then(result => {
                        const response = {
                            correlationId: msg.properties.correlationId,
                            path: result, // gives the file path
                            query_type: "compress"
                        };

                        logger.log({
                            level: "info",
                            message: `[ ${new Date()} ] Message sent: ${JSON.stringify(response)}`
                        });
                        console.log(
                            `[ ${new Date()} ] Message sent: ${JSON.stringify(response)}`,
                        );

                        ch.sendToQueue(
                            msg.properties.replyTo,
                            Buffer.from(JSON.stringify(response)), {
                                correlationId: msg.properties.correlationId,
                            },
                        );

                        ch.ack(msg);
                    });
                }
            });
        });
    })
    .catch(console.warn);
