FROM node:18-alpine

WORKDIR /app
RUN apk update \
    && apk --no-cache --update add bind-tools

COPY package* ./
RUN npm install

COPY . .
ENTRYPOINT [ "npm", "start" ]